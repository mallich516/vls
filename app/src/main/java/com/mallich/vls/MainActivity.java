package com.mallich.vls;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    boolean swipe = false;
    int timeLimit = 6000;
    int seconds = 1000;
    private String imeiNumber = "";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout relativeLayout = findViewById(R.id.relativeLayout);
        final TextView textView = findViewById(R.id.logoTextView);

        final RecyclerView recyclerView = findViewById(R.id.lanchRV);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        final ProgressBar progressBar = findViewById(R.id.logoProgressBar);

        checkuser();

        textView.setText(String.valueOf(1));
        new Thread() {
            @Override
            public void run() {
                try {
                    while (!currentThread().isInterrupted()) {
                        Thread.sleep(1000);
                        seconds += 1000;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (seconds < timeLimit) {
                                    textView.setText(String.valueOf(seconds / 1000));
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    textView.setPadding(50, 5, 50, 5);
                                    textView.setText("Login");
                                    swipe = true;
                                    return;
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        String json = "";
        try {
            InputStream is = this.getAssets().open("news_feeds.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            JSONArray jsonArray = jsonObject1.getJSONArray("newsFeeds");

            ArrayList<LaunchModel> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject2 = (JSONObject) jsonArray.get(i);
                LaunchModel model = new LaunchModel();
                model.setTitle(jsonObject2.getString("posted_by"));
                model.setDescription(jsonObject2.getString("feed_description"));
                model.setImageUrl(jsonObject2.getString("image_logo"));
                model.setTime(jsonObject2.getString("posted_date"));
                arrayList.add(model);
            }
            LaunchAdapter adapter = new LaunchAdapter(this, arrayList);
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Loginpage.class));
            }
        });

    }

    private void checkuser() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = {Manifest.permission.READ_PHONE_STATE};
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permissions, 16);
            }
        } else {
            try {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                imeiNumber = telephonyManager.getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            imeiNumber = telephonyManager.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        checkImei(imeiNumber);
    }

    private void checkImei(String imeiNumber) {

        boolean valid = false;

        ArrayList<String> imeiList = new ArrayList<>();
        imeiList.add("864074048556997");
        imeiList.add("864074048556996");
        imeiList.add("864074048556995");
        imeiList.add("864074048556994");
        imeiList.add("864074048556993");

        for(int i = 0; i < imeiList.size(); i++) {
            if (imeiList.get(i).equals(imeiNumber)) {
                valid = true;
            }
        }

        if (!valid) {
            startActivity(new Intent(MainActivity.this, InValidUserActivity.class));
        } else {
            return;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 16:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    try {
                        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        imeiNumber = telephonyManager.getDeviceId();
                        checkImei(imeiNumber);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
        }
    }

}
