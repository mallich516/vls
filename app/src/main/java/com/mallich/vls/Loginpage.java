package com.mallich.vls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Loginpage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }


    public void login(View view) {
        Toast.makeText(this, "Login Clicked", Toast.LENGTH_SHORT).show();
    }

    public void signup(View view) {
        startActivity(new Intent(Loginpage.this, Register.class));
    }
}
