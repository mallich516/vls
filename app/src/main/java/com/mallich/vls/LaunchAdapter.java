package com.mallich.vls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

public class LaunchAdapter extends RecyclerView.Adapter<LaunchAdapter.Launch> {

    Context context;
    ArrayList<LaunchModel> list;

    public LaunchAdapter(Context context, ArrayList<LaunchModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Launch onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Launch(LayoutInflater.from(context).inflate(R.layout.launch_rv_row, parent, false));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull Launch holder, int position) {
        final LaunchModel model = list.get(position);

        Glide.with(context.getApplicationContext()).load(model.getImageUrl()).into(holder.image);

        holder.title.setText(model.getTitle());
        holder.desc.setText(model.getDescription());
        holder.time.setText(model.getTime());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LaunchDescription.class);
                intent.putExtra("imageUrl", model.getImageUrl());
                intent.putExtra("title", model.getTitle());
                intent.putExtra("desc", model.getDescription());
                context.startActivity(intent);
            }
        });

//        holder.card.setOnTouchListener(new OnSwipeTouchListener(context) {
//            @Override
//            public void onLeftSwipe() {
//                context.startActivity(new Intent(context, Loginpage.class));
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Launch extends RecyclerView.ViewHolder {
        RoundedImageView image;
        TextView title, desc, time;
        MaterialCardView card;

        public Launch(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.launchRowImageView);
            title = itemView.findViewById(R.id.launchRowHeader);
            desc = itemView.findViewById(R.id.launchRowDesc);
            time = itemView.findViewById(R.id.launchRowTime);
            card = itemView.findViewById(R.id.launchRowCard);
        }
    }
}
