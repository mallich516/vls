package com.mallich.vls;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.MaterialToolbar;

public class LaunchDescription extends AppCompatActivity {

    ImageView image;
    TextView title, desc;
    MaterialToolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_description);

        image = findViewById(R.id.launchDescImage);
        title = findViewById(R.id.launchDescHeader);
        desc = findViewById(R.id.launchDescDesc);
        toolbar = findViewById(R.id.launchDescToolBar);

        String imageUrl = getIntent().getStringExtra("imageUrl");
        String header = getIntent().getStringExtra("title");
        String description = getIntent().getStringExtra("desc");

        Glide.with(getApplicationContext()).load(imageUrl).into(image);
        title.setText(header);
        desc.setText(description);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
